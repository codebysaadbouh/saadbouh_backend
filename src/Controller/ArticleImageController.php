<?php

namespace App\Controller;

use App\Entity\Article;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ArticleImageController extends AbstractController
{
    public function __invoke(Request $request): Article
    {
        $article = $request->attributes->get('data');
        if (!$article instanceof Article) {
            throw new \RuntimeException('Article not found');
        }
        $article->setFile($request->files->get('file'));
        $article->setUpdatedAt(new \DateTimeImmutable());
        return $article;
    }
}