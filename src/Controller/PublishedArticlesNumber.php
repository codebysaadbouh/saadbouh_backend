<?php

namespace App\Controller;

use App\Entity\Article;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;

#[AsController]
class PublishedArticlesNumber extends AbstractController
{
    public function __invoke(EntityManagerInterface $em, Request $request): JsonResponse
    {
        $publishedArticles          = $em->getRepository(Article::class)->findBy(['isPublished' => true]);
        $unpublishedArticles        = $em->getRepository(Article::class)->findBy(['isPublished' => false]);
        $articlesWithoutFilePath    = $em->getRepository(Article::class)->findBy(['filePath' => null]);
        return $this->json(
            [
                'publishedArticles' => count($publishedArticles),
                'unpublishedArticles' => count($unpublishedArticles),
                'articlesWithoutFilePath' => count($articlesWithoutFilePath),
                'articlesWithFilePath' => (count($publishedArticles)+count($unpublishedArticles)) - count($articlesWithoutFilePath),
            ]
        );
    }

}