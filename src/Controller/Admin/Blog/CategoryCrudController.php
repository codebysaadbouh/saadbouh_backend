<?php

namespace App\Controller\Admin\Blog;

use App\Entity\Category;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class CategoryCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Category::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setPageTitle('index', 'Catégories')
            ->setEntityLabelInSingular('Catégorie')
            ->setEntityLabelInPlural('Catégories')
            ->setDateFormat('dd/MM/yyyy')
            ->setPageTitle('new', 'Créer une nouvelle catégorie')
            ->setPageTitle('edit', 'Modifier la catégorie')
            ->setPageTitle('detail', 'Détail de la catégorie');
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->OnlyOnIndex(),
            TextField::new('title', 'Titre de la catégorie')->renderAsHtml(),
            TextField::new('slug', 'Slug de la catégorie')->onlyWhenUpdating(),
            TextEditorField::new('description'),
            DateField::new('createdAt', 'Créé le')->onlyOnIndex(),
            DateField::new('updatedAt', 'Modifié le')->onlyOnIndex(),
        ];
    }

}
