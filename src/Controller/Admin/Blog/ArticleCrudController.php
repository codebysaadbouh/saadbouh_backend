<?php

namespace App\Controller\Admin\Blog;

use App\Entity\Article;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Vich\UploaderBundle\Form\Type\VichImageType;

class ArticleCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Article::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setPageTitle('index', 'Articles')
            ->setEntityLabelInSingular('Article')
            ->setEntityLabelInPlural('Articles')
            ->setDateFormat('dd/MM/yyyy')
            ->setPageTitle('new', 'Créer une nouvelle catégorie')
            ->setPageTitle('edit', 'Modifier la catégorie')
            ->setPageTitle('detail', 'Détail de la catégorie')
            ->addFormTheme('@FOSCKEditor/Form/ckeditor_widget.html.twig');
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->OnlyOnIndex(),
            TextField::new('title', 'Titre de l\'article'),
            AssociationField::new('category', 'Catégorie'),
            TextField::new('file', "Affiche de l'article")->setFormType(VichImageType::class)->onlyOnForms(),
            ImageField::new('filePath', 'Image')->setBasePath('/images/articles')->onlyOnIndex(),
            TextEditorField::new('intro', 'Introduction'),
            TextEditorField::new('content', "Contenu de l'article")->hideOnIndex()->setFormType(CKEditorType::class),
            BooleanField::new('isPublished', 'Publié'),
            TextField::new('slug', 'Slug de la catégorie')->onlyWhenUpdating(),
            DateField::new('createdAt', 'Créé le')->onlyOnIndex(),
            DateField::new('updatedAt', 'Modifié le')->onlyOnIndex(),
        ];
    }
}
