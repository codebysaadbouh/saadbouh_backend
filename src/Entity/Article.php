<?php

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\ExistsFilter;
use ApiPlatform\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Link;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Put;
use App\Controller\ArticleImageController;
use App\Controller\PublishedArticlesNumber;
use App\Controller\PublishStatusArticleController;
use App\Repository\ArticleRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert; // Symfony's built-in constraints
use Vich\UploaderBundle\Mapping\Annotation as Vich;

#[ORM\Entity(repositoryClass: ArticleRepository::class)]
#[Vich\Uploadable]
#[ApiResource(
    normalizationContext: [
        'groups' => ['article_read'],
        'openapi_definition_name' => 'Collection',
    ],
    denormalizationContext: [
        'groups' => ['article_write'],
        'openapi_definition_name' => 'Write',
        ],
    paginationEnabled: true,
    paginationItemsPerPage: 15,
)]
#[ApiResource(
    operations: [
        new Get(
            openapiContext: [
                'summary' => 'Get an article',
                'description' => 'Récupération d\'un article en fonction de son ID',
            ]),
        new GetCollection(
            openapiContext: [
                'summary' => 'Get all articles',
                'description' => 'Récupération de tous les articles',
            ]),
        new Post(
            openapiContext: [
                'summary' => 'Create an article',
                'description' => 'Création d\'un article',
            ]),
        new Put(
            openapiContext: [
                'summary' => 'Update an article',
                'description' => 'Mise à jour d\'un article',
            ]),
        new Delete(
            openapiContext: [
                'summary' => 'Delete an article',
                'description' => 'Suppression d\'un article en fonction de son ID',
            ]),
    ],
    order: ['createdAt' => 'DESC']
)]
#[ApiResource(
    uriTemplate: '/categories/{categoryID}/articles/{id}',
    operations: [new Get(
        openapiContext: [
            'summary' => 'get article of a specific category',
            'description' => 'Récupération de tous les articles d\'une catégorie en fonction de son ID',
        ])],
    uriVariables: [
        'categoryID' => new Link(toProperty: 'category', fromClass: Category::class),
        'id' => new Link(fromClass: Article::class),
    ],
)]
#[ApiResource(
    uriTemplate: '/categories/{categoryID}/articles',
    operations: [new GetCollection(
        openapiContext: [
            'summary' => 'get all articles of a specific category',
            'description' => 'Récupération d\'un article, d\'une catégorie en fonction de son ID (categoryID)',
    ])],
    uriVariables: [
        'categoryID' => new Link(toProperty: 'category', fromClass: Category::class),
    ],
)]

#[ApiResource(
    operations: [
        new Put(
            uriTemplate: '/articles/{id}/publish',
            controller: PublishStatusArticleController::class,
            openapiContext: [
                'summary' => 'Publish an article by changing the status',
                'description' => 'Publication d\'un article en fonction de son ID',
                'requestBody'=> [
                    'content' => [
                        'application/json' => [
                            'schema' => []
                        ]
                    ]
                ]
            ],
            name: 'publish',
    )],
)]

#[ApiResource(
    //get articles published number
    operations: [
        new GetCollection(
            uriTemplate: '/articlesFigures',
            controller: PublishedArticlesNumber::class,
            openapiContext: [
                'summary' => 'Get the number of published articles',
                'description' => 'Récupération du nombre d\'articles publiés',
            ],
            name: 'published_articles_number',
        ),
    ],
)]

#[ApiResource(
    operations: array(
        new Post(
            uriTemplate: '/articles/{id}/image',
            controller: ArticleImageController::class,
            openapiContext: array(
                'summary' => 'Upload an image for an article',
                'description' => 'Upload d\'une image pour un article en fonction de son ID',
                'requestBody'=> array(
                    'content' => array(
                        'multipart/form-data' => array(
                            'schema' => array(
                                'type' => 'object',
                                'properties' => array(
                                    'file' => array(
                                        'type' => 'string',
                                        'format' => 'binary',
                                    )
                                )
                            )
                        )
                    )
                )
            ),
            deserialize: false,
        )
    )
)]

#[ApiFilter(SearchFilter::class, properties:['title'=> 'partial'])]
#[ApiFilter(ExistsFilter::class, properties:['filePath'])]
#[ApiFilter(DateFilter::class, properties: ['createdAt'])]
#[ApiFilter(BooleanFilter::class, properties: ['isPublished'])]
#[ApiFilter(OrderFilter::class, properties: ['title', 'updatedAt'], arguments: ['orderParameterName' => 'order'])]
#[ApiFilter(SearchFilter::class, properties:['slug'=> 'exact'])]
class Article
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['article_read', 'category_read','user_read'])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank(message: 'The title is mandatory')]
    #[Assert\Length(min: 4, minMessage: 'The title must be at least 4 characters long')]
    #[Groups(['article_read', 'category_read','article_write', 'user_read'])]
    private ?string $title = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Assert\NotBlank(message: 'The intro is mandatory')]
    #[Assert\Length(min: 100, minMessage: 'The intro must be at least 100 characters long')]
    #[Groups(['article_read','category_read', 'article_write', 'user_read'])]
    private ?string $intro = null;

    #[ORM\Column(type: Types::TEXT)]
    #[Assert\NotBlank(message: 'The content is mandatory')]
    #[Assert\Length(min: 200, minMessage: 'The content must be at least 250 characters long')]
    #[Groups(['article_read','category_read', 'article_write', 'user_read'])]
    private ?string $content = null;

    #[Gedmo\Timestampable(on: 'create')]
    #[Groups(['article_read'])]
    #[ORM\Column]
    private ?\DateTimeImmutable $createdAt = null;

    #[Gedmo\Timestampable(on: 'update')]
    #[Groups(['article_read'])]
    #[ORM\Column]
    private ?\DateTimeImmutable $updatedAt = null;

    #[ORM\ManyToOne(inversedBy: 'article')]
    #[ORM\JoinColumn(nullable: false)]
    #[Assert\NotBlank(message: 'The category is mandatory')]
    #[Groups(['article_read', 'article_write'])]
    private ?Category $category = null;

    #[ORM\Column(options: ['default' => false])]
    #[
        Groups(['article_read','category_read', 'article_write']),
        ApiProperty(openapiContext: [
            'type' => 'boolean',
            'description' => 'Mettre en ligne ou non l\'article',
        ]),
    ]
    private ?bool $isPublished = null;

    #[ORM\ManyToOne(inversedBy: 'articles')]
    #[Groups(['article_read','category_read'])]
    private ?User $author = null;

    #[Vich\UploadableField(mapping: 'article_image', fileNameProperty: 'filePath')]
    #[Assert\NotNull(groups: ['article_image_create'])]
    #[Assert\Image(maxSize: '2M', maxSizeMessage: 'The image is too large ({{ size }} {{ suffix }}). Allowed maximum size is {{ limit }} {{ suffix }}')]
    private ?File $file = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(['article_read','category_read', 'article_write'])]
    private ?string $filePath = null;

    #[Groups(['article_read','category_read'])]
    private ?string $fileUrl = null;

    #[ORM\Column(length: 255)]
    #[Groups(['article_read','category_read'])]
    #[Gedmo\Slug(fields: ['title'], updatable: true, unique: true)]
    private ?string $slug = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getIntro(): ?string
    {
        return $this->intro;
    }

    public function setIntro(?string $intro): self
    {
        $this->intro = $intro;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeImmutable $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function isIsPublished(): ?bool
    {
        return $this->isPublished;
    }

    public function setIsPublished(bool $isPublished): self
    {
        $this->isPublished = $isPublished;

        return $this;
    }

    public function getAuthor(): ?User
    {
        return $this->author;
    }

    public function setAuthor(?User $author): self
    {
        $this->author = $author;

        return $this;
    }

    public function getFile(): ?File
    {
        return $this->file;
    }

    public function setFile(?File $file): self
    {
        $this->file = $file;
        if($file) {
            $this->setUpdatedAt(new \DateTimeImmutable());
        }
        return $this;
    }

    public function getFilePath(): ?string
    {
        return $this->filePath;
    }

    public function setFilePath(?string $filePath): self
    {
        $this->filePath = $filePath;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getFileUrl(): ?string
    {
        return $this->fileUrl;
    }

    /**
     * @param string|null $fileUrl
     */
    public function setFileUrl(?string $fileUrl): void
    {
        $this->fileUrl = $fileUrl;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }
}
