<?php

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Put;
use App\Repository\CategoryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert; // Symfony's built-in constraints


#[ORM\Entity(repositoryClass: CategoryRepository::class)]
#[ApiResource(
    operations: [
        new Get(
            openapiContext: [
                'summary' => 'Get a category',
                'description' => 'Récupération d\'une catégorie en fonction de son ID',
            ]),
        new GetCollection(
            openapiContext: [
                'summary' => 'Get all categories',
                'description' => 'Récupération de toutes les catégories',
            ]),
        new Post(
            openapiContext: [
                'summary' => 'Create a category',
                'description' => 'Création d\'une catégorie',
            ]),
        new Put(
            openapiContext: [
                'summary' => 'Update a category',
                'description' => 'Mise à jour d\'une catégorie',
            ]),
        new Delete(
            openapiContext: [
                'summary' => 'Delete a category',
                'description' => 'Suppression d\'une catégorie en fonction de son ID',
            ]),
    ],
    normalizationContext: ['groups' => ['category_read']],
    denormalizationContext: ['groups' => ['category_write']],
    order: ['title' => 'ASC']
)]

#[ApiFilter(SearchFilter::class, properties:['title'=> 'partial'])]
#[ApiFilter(SearchFilter::class, properties:['slug'=> 'exact'])]
class Category
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['category_read', 'article_read'])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank(message: 'The title is mandatory')]
    #[Groups(['category_read', 'article_read', 'category_write'])]
    private ?string $title = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Groups(['category_read', 'article_read', 'category_write'])]
    private ?string $description = null;

    #[ORM\OneToMany(mappedBy: 'category', targetEntity: Article::class)]
    private Collection $article;

    #[ORM\Column(length: 255)]
    #[Gedmo\Slug(fields: ['title'], updatable: false, unique: true)]
    #[Groups(['category_read', 'article_read'])]
    private ?string $slug = null;

    #[ORM\Column]
    #[Gedmo\Timestampable(on: 'update')]
    #[Groups(['category_read', 'article_read'])]
    private ?\DateTimeImmutable $createdAt = null;

    #[ORM\Column]
    #[Gedmo\Timestampable(on: 'update')]
    #[Groups(['category_read', 'article_read'])]
    private ?\DateTimeImmutable $updatedAt = null;

    public function __construct()
    {
        $this->article = new ArrayCollection();
    }

    #[Groups(['category_read'])]
    public function getArticleNumbers(): int
    {
        return $this->getArticle()->count();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection<int, Article>
     */
    public function getArticle(): Collection
    {
        return $this->article;
    }

    public function addArticle(Article $article): self
    {
        if (!$this->article->contains($article)) {
            $this->article->add($article);
            $article->setCategory($this);
        }

        return $this;
    }

    public function removeArticle(Article $article): self
    {
        if ($this->article->removeElement($article)) {
            // set the owning side to null (unless already changed)
            if ($article->getCategory() === $this) {
                $article->setCategory(null);
            }
        }

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeImmutable $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function __toString(): string
    {
        return $this->title;
    }
}
