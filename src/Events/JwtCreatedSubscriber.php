<?php

namespace App\Events;

use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTCreatedEvent;
use Symfony\Component\HttpFoundation\RequestStack;

class JwtCreatedSubscriber
{
    /**
     * @var RequestStack
     */
    private RequestStack $requestStack;

    /**
     * @param RequestStack $requestStack
     */
    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    /**
     * @param JWTCreatedEvent $event
     *
     * @return void
     */
    public function onJWTCreated(JWTCreatedEvent $event): void
    {

    }

    public function updateJwtData(JWTCreatedEvent $event): void
    {
        $request = $this->requestStack->getCurrentRequest();

        // 1. obtenir l'utilisateur (prénom, nom, courriel)
        $user = $event->getUser();

        // Enrich the dara to contain this data
        $data = $event->getData();

        $data['id'] = $user->getId();
        $data['firstname'] = $user->getFirstName();
        $data['lastname'] = $user->getLastName();
        $data['email'] = $user->getEmail();

        $expiration = new \DateTime('+1 day');
        $expiration->setTime(2, 0, 0);

        $data['exp'] = $expiration->getTimestamp();

        $event->setData($data);
    }
}