<?php

namespace App\Events;

use ApiPlatform\Symfony\EventListener\EventPriorities;
use App\Entity\Article;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Security;

class ArticleUserSubscriber implements EventSubscriberInterface
{
    private Security $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }


    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::VIEW => ['setUserForArticle', EventPriorities::PRE_VALIDATE]
        ];
    }

    public function setUserForArticle(ViewEvent $event)
    {
        $article   =   $event->getControllerResult();
        $method    =   $event->getRequest()->getMethod();

        if(($article instanceof Article) && ($method === "POST")){
            // Get the current user
            $user = $this->security->getUser();
            // Assign the user to the article
            $article->setAuthor($user);
        }
    }
}