<?php

namespace App\DataFixtures;

use App\Entity\Article;
use App\Entity\Category;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    /**
     * Password Encoder
     * @var UserPasswordHasherInterface
     */
    private UserPasswordHasherInterface $encoder;
    public function __construct(UserPasswordHasherInterface $encoder)
    {
        $this->encoder = $encoder;
    }


    public function load(ObjectManager $manager): void
    {


        $Faker = new Factory();
        $fakeData = $Faker->create('fr_FR');
        $User = new User();
        $User->setFirstName('Cheikh')
            ->setLastName('SOW')
            ->setEmail('saadbouh.code@gmail.com')
            ->setPassword($this->encoder->hashPassword($User, 'password'));
        $manager->persist($User);

        for($i = 0; $i < 10; $i++) {
            $Category = new Category();
            $Category->setTitle($fakeData->sentence(mt_rand(3, 4)));
            $Category->setDescription($fakeData->realText(200,2));
            $manager->persist($Category);

            for ($j = 0; $j < mt_rand(15, 30); $j++) {
                $Article = new Article();
                $Article->setTitle($fakeData->sentence(mt_rand(3, 4)));
                $Article->setIntro($fakeData->paragraph(mt_rand(3, 4), false));
                $Article->setContent($fakeData->realText(500,2));
                $Article->setCategory($Category);
                $Article->setIsPublished(mt_rand(0, 1));
                $Article->setAuthor($User);
                $manager->persist($Article);
            }
        }
        $manager->flush();
    }
}
