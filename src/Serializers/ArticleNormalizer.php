<?php

namespace App\Serializers;

use App\Entity\Article;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface as ContextAwareNormalizerInterfaceAlias;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Vich\UploaderBundle\Storage\StorageInterface;

class ArticleNormalizer  implements ContextAwareNormalizerInterfaceAlias, NormalizerAwareInterface
{
    use NormalizerAwareTrait;
    
    private const ALREADY_CALLED = 'ArticleImageNormalizerAlreadyCalled';
    private StorageInterface $storage;

    public function __construct(StorageInterface $storage)
    {
        $this->storage = $storage;
    }


    public function supportsNormalization(mixed $data, string $format = null, array $context = []): bool
    {
        return !isset ($context[self::ALREADY_CALLED]) && $data instanceof Article;
    }

    /**
     * @param Article $object
     * @throws ExceptionInterface
     */
    public function normalize(mixed $object, string $format = null, array $context = []): float|array|\ArrayObject|bool|int|string|null
    {
        $object->setFileUrl($this->storage->resolveUri($object, 'file'));
        $context[self::ALREADY_CALLED] = true;
        return $this->normalizer->normalize($object, $format, $context);

    }
}